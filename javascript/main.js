/*
 *  Desc.   : page javascript qui va gérer les 
 *            appels aux web services, gérer des fichiers 
 *            jsons et renvoyer le résultat du traitement à l'utilisateur
 *  Erreurs : ...
 *  Prog    : nuissances_sonores -> main.js
 *  Updates : ...
*/
var calculer = new Vue({
  el: '#resultat',
  data: {
    resultat: ''
  },
  methods: {
    reverseMessage: function() {
      this.resultat = this.resultat.split('').reverse().join('')
    }
  }
})
var destination = document.getElementById("resultat");
var vectorSource = new ol.source.Vector();
var map = new ol.Map();

/*
 * Calculate distance between given point and all the data from backEnd
*/
function btnCalcul() {
  let saisieLat = document.getElementById("lat").value * 1;
  let saisieLon = document.getElementById("lon").value * 1;
  let saisieKm  = document.getElementById("dist").value * 1;

  callWS(saisieKm, saisieLat, saisieLon);
}

/*
 * Calculate distance between two given points
 * @param {number} lat1 - latitude of the first point
 * @param {number} lon1 - longitude of the first point
 * @param {number} lat2 - latitude of the second point
 * @param {number} lon2 - longitude of the second point
*/
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  const EarthRadius = 6371;   // Radius of the earth in km.
  let dLat = deg2rad(lat2 - lat1);  //deg2rad below.
  let dLon = deg2rad(lon2 - lon1);
  let a = Math.sin(dLat / 2.0) * Math.sin(dLat / 2.0) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2.0) * Math.sin(dLon / 2.0);
  let c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = EarthRadius * c;  // distance in km.
  return d;
}

/*
 * Convert degree to radian
 * @param {number} deg - angle in degree
*/
function deg2rad(deg){
  return deg * (Math.PI/180);
}

/*
 * Tranform JSON into array
 * @param {JSON data} json - JSON data
*/
function json2array(json){
  let result = [];
  let keys = Object.keys(json);
  keys.forEach(function(key){
    result.push(json[key]);
  });
  return result;
}

// Read airport data

/*
 * call web service method from M. Mathieu
 * @param {number} Km - distance in KM
 * @param {number} Latitude - given latitude of a point
 * @param {number} Longitude - given longitude of a point
*/
function callWS(Km, Latitude, Longitude) {
  const URL = "http://localhost/nuisances_sonores/php/backEnd.php";
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == XMLHttpRequest.DONE) {
          if (this.status == 200) {
            let list_flights = json2array(JSON.parse(this.responseText));
            let lat = list_flights[0];  // latitude array
            let lon = list_flights[1];  // longitude array
            let names = list_flights[2];// airport name array
            let result = [];
            let distance = [];
            // clean marker's layer
            vectorSource.clear()
            // cross given JSON data from backEnd, and obtain their distance from the given point
            for(let i = 0; i < lat.length; i++) {
              distance = getDistanceFromLatLonInKm(lat[i],lon[i], Latitude, Longitude);
              // Verify if airport is in the radius of the given distance
              if (distance <= Km){
                // round the number before displaying
                distance = distance.toFixed(1);
                result.push(names[i] + " (distance : " + distance + " km)");
                addMarker(lat[i], lon[i], names[i]);
              }
            }
            // Display airport(s) in a list
            destination.innerHTML = '<ul class="list-group list-group-flush"><li class="list-group-item disabled">Liste des aéroports</li>';
            for (let index = 0; index < result.length; index++) {
              const element = result[index];
              destination.innerHTML += '<li class="list-group-item">' + element + '</li>';
            }
            destination.innerHTML += "</ul>";
          } else {
            console.log("error");
            return "error";
          }
      }
  };
  xhttp.open("GET", URL, false);
  xhttp.send();
}

// Open Street Map / OpenLayer

/*
 * Create a map center at a given point
 * @param {number} lat - latitude of the center
 * @param {number} lon - longitude of the center
*/
function createMap(lat, lon){
  map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      // center on...
      center: ol.proj.fromLonLat([lon, lat]),
      zoom: 4
    })
  });
  var vectorLayer = new ol.layer.Vector({source: vectorSource});
  map.addLayer(vectorLayer);
}

/*
 * method that set style for the marker's label
*/
function setMarkerStyle() {
  let style = new ol.style.Style({
    image: new ol.style.Icon({
      anchor: [0.5, 46],
      anchorXUnits: 'fraction',
      anchorYUnits: 'pixels',
      opacity: 0.75,
      src: './img/icon.png'
    }),
    text: new ol.style.Text({
      font: '12px Calibri,sans-serif',
      fill: new ol.style.Fill({color: '#000'}),
      stroke: new ol.style.Stroke({
        color: '#fff', width: 2
      }),
      text: "unknown"
    })
  });
  return style;
}

/*
 * method that set a marker on a given lat/lon point
 * @param {number} lat - latitude of the marker point
 * @param {number} lon - longitude of the marker point
 * @param {string} name - label of the marker
*/
function addMarker(lat, lon, name) {
  let point =  ol.proj.fromLonLat([lon, lat]);
  //let point = ol.proj.transform(lon, lat, 'EPSG:4326', 'EPSG:3857');  // World Geodetic System 1984 used in GPS -> NE FONCTIONNE PAS
  let feature = new ol.Feature({geometry: new ol.geom.Point(point)});
  let style = setMarkerStyle();
  style.getText().setText(name);
  feature.setStyle(style);
  vectorSource.addFeature(feature);
}

// Geolocation

/*
 * call Web Service to obtain info from IP addr
*/
function ipLookUp() {
  // using fetch rather than ajax to avoid override
    fetch('http://ip-api.com/json').then(
      function success(response) {
        return response.json();
      }).then(function(data) {
        createMap(data['lat'], data['lon']);
      });
}

// manage the map display if user accept geolocation
function geolocation(){
  if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        function success(position) {
          createMap(position.coords.latitude, position.coords.longitude);        
        },
        function error(error_message) {
          ipLookUp();
        }
      );
  } else {
    ipLookUp();
  }
}

// Tests

/*
 * Test with CERN data
*/
function testCern10Km() {
  const lat = 46.234120;
  const lon = 6.052646;
  const km  = 10;
  callWS(km, lat, lon);
}

/*
* Test with Kloten Bahnhof data
*/
function testKloten10Km() {
  const lat = 47.448030;
  const lon = 8.583181;
  const km  = 10;
  callWS(km, lat, lon);
}

/*
* Test with Luxembourg garden data
*/
function testJardinLuxembourg15Km() {
  const lat = 48.847554;
  const lon = 2.336390;
  const km  = 15;
  callWS(km, lat, lon);  
}