/*
 * Desc.    :   scripts de tests
 * Prog.    :   nuisances_sonores -> test.js
*/
 /*
  * Test with CERN data
 */
  function testCern10Km() {
    const lat = 46.234120;
    const lon = 6.052646;
    const km  = 10;
    callWS(km, lat, lon);
  }
  
 /*
  * Test with Kloten Bahnhof data
 */
  function testKloten10Km() {
    const lat = 47.448030;
    const lon = 8.583181;
    const km  = 10;
    callWS(km, lat, lon);
  }
  
 /*
  * Test with Luxembourg garden data
 */
  function testJardinLuxembourg15Km() {
    const lat = 48.847554;
    const lon = 2.336390;
    const km  = 15;
    callWS(km, lat, lon);  
  }