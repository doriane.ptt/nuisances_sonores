<?php
/*
 *  Desc.   :   back end page
 *  Erreurs :   ...
 *  Prog    :   nuissances_sonores -> backEnd.php
 *  Update  :   ...
*/
header('Content-Type: application/json');

function callAPI($_url){
    $curl = curl_init($_url);   // init a new session and return a cURL handle ready to be used
    // setting options for a cURL session handle
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // return the transfer as a string instead of outputting
    curl_setopt($curl, CURLOPT_HEADER, 0); // include the header in the output
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // don't check the names
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // stop cURL from verifying the peer's certificate
    // execute the given cURL session
    $APIoutput = curl_exec($curl);
    curl_close($curl);
    return $APIoutput;
}
#region init
$url = "https://www.flightradar24.com/_json/airports.php";
$output = callAPI($url);
$latitudes = array();
$longitudes = array();
$coordinates = array("lat" => array(), "lon" => array());
// return assoc array
$output = json_decode($output, true);   // decode the returning JSON file
#endregion
#region processing
// fill an array with coordinates from an extern API
foreach ($output as $num => $col) {
    if ($col == $output['rows']) {
        foreach ($col as $key => $value) {
            if ($value["lat"]) {
                $coordinates["lat"][] = $value['lat'];
            }
            if ($value["lon"]) {
                $coordinates['lon'][] = $value['lon'];
            }
            if ($value['name']) {
                $coordinates['name'][] = $value['name'];
            }
        }
    }
}
#endregion
// enconding the array in json file
echo json_encode($coordinates);